from flask import Flask, request
app = Flask(__name__)

@app.route('/hello', methods=['POST'])
def hello_world():
   json_data = request.get_json()
   name = json_data['name']
   return 'Hello ' + name

if __name__ == '__main__':
   app.run()