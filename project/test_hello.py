import unittest

from hello import app

class HelloTest(unittest.TestCase):
    def setUp(self):
        # creates a test client
        self.app = app.test_client()
        # propagate the exceptions to the test client
        self.app.testing = True 

    def tearDown(self):
        pass

    def test_hello1(self):
        response = self.app.post('/hello', json={
            'name': 'Flask'
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_data(as_text=True), "Hello Flask")

    def test_hello2(self):
        response = self.app.post('/hello', json={
            'name': 'World'
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.get_data(as_text=True), "Hello World")

if __name__ == '__main__':
    unittest.main()